from setuptools import setup, find_packages
import codecs
import os

here = os.path.abspath(os.path.dirname(__file__))

with codecs.open(os.path.join(here, "README.md"), encoding="utf-8") as fh:
    long_description = "\n" + fh.read()

VERSION = '0.0.1'
DESCRIPTION = 'programa para jugar alas cartas'

# Setting up
setup(
    name="AhmerCards",
    version=VERSION,
    author="Ahmer Muhammad",
    author_email="ahmerishere@gmail.com",
    description='programa para jugar alas cartas',
    packages=['Cardspck'],
    license='MIT',
    install_requires=[],
    keywords=[],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    zip_safe=False
)
